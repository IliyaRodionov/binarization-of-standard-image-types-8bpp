﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <io.h>
#include <math.h>
#include <rpc.h>
#include <wingdi.h>
#include "FreeImage.h"


/*
 * Данный метод возвращает указатель на массив яркостей
 * */
double** getArrayBrightnessPixel(char* in_image_path, double** img_brightness, int* size_image_height, int* size_image_width);
/*
 * Метод niblack возвращает указатель на массив данных бинаризации изображения
 * на вход подается:
 * - указатель на массив яркостей
 * - указатель на массив бинаризации
 * - размеры изображения
 * - размер окна для работы алгоритма Ниблэка
 * - значение смещения "к" для работы алгоритма Ниблэка
 * */
char** niblack(double** img_brightness, char** img_binary, int size_image_height, int size_image_width, int size_window,
	double k);
/*
 * Метод threshold возвращает 1 или 0 в зависимоти результата работы алгоритма Ниблэка. Сравнивается реальная яркость пикселя с пороговым
 * значением яркости, которое расчитывается по алгоритму Ниблэка
 * На вход принимаются:
 * - i, j значения индекса сдвига окна. Иллюстрация алгоритма движения окна представлена в документе "Сопроводительное пиьсмо".
 * когда значения индекса пикселя превыщаю середину окна, они изменяются и перемещают окно.
 * - w - размер окна
 * - к - коэффициент(смещение)
 * - n размер изображения ( высота - количество строк)
 * - m размер изображения (длина - количество столбцов)
 * */
char threshold(double** img_brightness, int number_of_columns_in_an_array_brightness, int i, int j, int w, double k, int n, int m);
/*
 * Записывает обработанное изображение в новый файл
 * На вход принимается:
 * - путь до входного изображения
 * - путь до выодного изображения
 * - массив бинаризации
 * */
void write_a_new_binarization_image(char* in_image_path, char* out_image_path, char** img_binary);
/*
 * Расчет среднего значения
 * */
double mean_local_area(int i, int j, int w, double** img_brightness, int number_of_columns_in_an_array_brightness);
/*
 * Расчет стандартного отклонения
 * */
double deviation_local_area(int i, int j, int w, double x_mean, double** img_brightness, int number_of_columns_in_an_array_brightness);
/*
Функции загрузки файла и вывода ошибок чтения
*/
FIBITMAP* GenericLoader(const char* lpszPathName, int flag);
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char* message);

/*
 * Освобождение памяти
 * */
void freeing_up_memory(void** A, size_t n);
/*
 * Функции вывода данных на экран
 * */
void dynamic_array_print_Char(char** array, size_t n, size_t m);
void dynamic_array_print_Double(double** array, size_t n, size_t m);

typedef struct __attribute__ {
	unsigned char  p;
} IMAGE;

int main(int argc, char * argv[]) {

	//Объявление стандартных параметров (входное изображение,выходное изображение, размер окна, значение коэфициента)
	char* name_input_image_file = NULL;
	char* name_output_image_file = NULL;
	int w = 15;
	double k = 0.2;


	if (argc == 5) {
		
		int len_in = strlen(argv[1]);
		int len_out = strlen(argv[4]);

		name_input_image_file = (char*)malloc(len_in * sizeof(char) + 1);
		name_output_image_file = (char*)malloc(len_out * sizeof(char) + 1);

		strcpy(name_input_image_file, argv[1]);
		w = atoi(argv[2]);
		k = atof(argv[3]);
		strcpy(name_output_image_file, argv[4]);

	} 
	else if (argc == 2) {

		char prefix_new_name[_MAX_FNAME] = "binarization_";

		int len_in = strlen(argv[1]);
		int len_out = len_in + strlen(prefix_new_name);

		name_input_image_file = (char*)malloc(len_in * sizeof(char) + 1);
		name_output_image_file = (char*)malloc(len_out * sizeof(char) + 1);

		strcpy(name_input_image_file, argv[1]);

		strcpy(name_output_image_file, strcat(prefix_new_name, argv[1]));
		
	} else {

		printf("Enter a name for the image or enter the parameters as described in the README!!");
		return 0;
	}
	
	int size_image_height = 0;
	int size_image_width = 0;

	////Указатель на массив яркостей
	double** img_brightness = NULL;
	img_brightness = getArrayBrightnessPixel(name_input_image_file, img_brightness, &size_image_height, &size_image_width);
	////dynamic_array_print_Double(img_brightness, size_image_height, size_image_width);

	////Указатель на массив бинаризации
	char** img_binary = NULL;
	img_binary = niblack(img_brightness, img_binary, size_image_height, size_image_width, w, k);
	////dynamic_array_print_Char(img_binary, size_image_height,size_image_width);

	////Записываем новый файл
    write_a_new_binarization_image(name_input_image_file, name_output_image_file, img_binary);
	

	freeing_up_memory(img_brightness, size_image_height);
	freeing_up_memory(img_binary, size_image_height);
	free(name_input_image_file);
	free(name_output_image_file);

	
	return 0;
}

double** getArrayBrightnessPixel(char* in_image_path, double** img_brightness, int* size_image_height, int* size_image_width) {

	//инициализируем обработчик ошибок
	FreeImage_SetOutputMessage(FreeImageErrorHandler);

	FIBITMAP* dib = GenericLoader(in_image_path, 0);

	RGBQUAD* pallete = NULL;
	
	int i = 0, j = 0, n = 0, index_palette = 0;

	BYTE* index = (BYTE*)malloc(sizeof(BYTE));
	
	double a = 0.2126, b = 0.7152, c = 0.0722;  //коэффициенты для рассчета яркости пикселя RGB

	if (dib) {

		*size_image_height = FreeImage_GetHeight(dib);
		*size_image_width = FreeImage_GetWidth(dib);
		
		if (FreeImage_GetBPP(dib) == 8) {
			// Build a greyscale palette
		 pallete = FreeImage_GetPalette(dib);
		}
		else {
			printf("Pixel depth is greater than 8 bits.Use an image with a pixel depth of 8bpp.");
			return 0;
		}


		//выделяем память для массива яркостей и сдвигаем указатель на начало пиксельных данных
		//img_brightness = (double*)malloc(bitmapinfoheader.biHeight*bitmapinfoheader.biWidth* sizeof(double));
		img_brightness = (double**)malloc(*size_image_height * sizeof(double*));
		for (int k = 0; k < *size_image_height; k++) {
			img_brightness[k] = (double*)malloc(*size_image_width * sizeof(double));
		}

		//цикл пробега по пиксельным данным
		for (i = 0; i < *size_image_height; i++) {
			for (j = 0; j < *size_image_width; j++) {

				//в случае 8-ми битного описания пикселя в файле bmp, в пиксельных данных хранится индекс цветовой палитры
				//по данному индексу, будет производиться обращение в массив структур палитры
				FreeImage_GetPixelIndex(dib, j, i, index);
				//index = (int)index;
				//рассчитываем значение яркости
				//и заполнаем клон-массив яркостей пикселей
				//*(img_brightness + i*bitmapinfoheader.biWidth + j) = (a * RGB_pixel[index_palette].rgbtRed) + (b * RGB_pixel[index_palette].rgbtGreen) + (c * RGB_pixel[index_palette].rgbtBlue);
				img_brightness[i][j] = (a * pallete[*index].rgbRed) + (b * pallete[*index].rgbGreen) + (c * pallete[*index].rgbBlue);

			}

		}

		printf("getArrayBrightnessPixel - done\n");
		FreeImage_Unload(dib);
		free(index);
		return img_brightness;

	} else {
		printf("Failed to open file!\n");
		return 0;
	}

}

char** niblack(double** img_brightness, char** img_binary, int size_image_height, int size_image_width, int size_window,
	double k) {

	int i = 0, j = 0;   //счетчики сдвига окна
	int n, m;   //индексы пробега по пиксельным данным

	//выделяем память для массива бинаризации
	//img_binary = (char *)malloc(size_image_height*size_image_width* sizeof(char));
	img_binary = (char**)malloc(size_image_height * sizeof(char*));
	for (int g = 0; g < size_image_height; g++) {
		img_binary[g] = (char*)malloc(size_image_width * sizeof(char));
	}

	int start_move_window = size_window / 2 + 1;  //считаем середину окна (только для нечетных значения размера окна)
	int start_move_window_y = start_move_window;
	int stop_move_window_y = size_image_height - 1 - start_move_window_y;
	int start_move_window_x = start_move_window; //остановка сдвига окна по горизонтали
	int stop_move_window_x = size_image_width - 1 - start_move_window;  //остановка сдвига окна по вертикали

	for (n = 0; n < size_image_height; n++) {

		if (n >= 0 && n < start_move_window_y) {
			i = 0;
		}
		else if (n >= start_move_window_y && n <= stop_move_window_y) {
			i += 1;
		}

		for (m = 0; m < size_image_width; m++) {

			if (m >= 0 && m < start_move_window_x) {
				j = 0;
			}
			else if (m >= start_move_window_x && m <= stop_move_window_x) {
				j += 1;
			}

			//*(img_binary + n*size_image_width + m) = threshold(img_brightness, size_image_width, i, j, size_window, k, n, m);
			img_binary[n][m] = threshold(img_brightness, size_image_width, i, j, size_window, k, n, m);

		}
	}

	printf("niblack - done\n");
	return img_binary;
}

char threshold(double** img_brightness, int number_of_columns_in_an_array_brightness, int i, int j, int w, double k, int n, int m) {

	double x_mean = 0;
	double d_deviation = 0;
	double value_threshold = 0;     //пороговое значение по алгоритму Niblack
	double I = img_brightness[n][m];    //значение пикселя, относительно которого считается пороговое значение и которое сравнивается с полученным пороговым значением


	x_mean = mean_local_area(i, j, w, img_brightness, number_of_columns_in_an_array_brightness);
	d_deviation = deviation_local_area(i, j, w, x_mean, img_brightness, number_of_columns_in_an_array_brightness);

	value_threshold = x_mean + k * d_deviation;

	//бинаризация с нижним порогом
	if (I >= value_threshold) {

		return 0;

	}
	else if (I < value_threshold) {

		return 1;

	}

	/*
	//бинаризация с верхним порогом
	if(I <= value_threshold){

		return 0;

	} else if(I > value_threshold){

		return 1;

	}
	*/
}

void write_a_new_binarization_image(char* in_image_path, char* out_image_path, char** img_binary) {

	//инициализируем обработчик ошибок
	FreeImage_SetOutputMessage(FreeImageErrorHandler);

	FIBITMAP* dib = GenericLoader(in_image_path, 0);

	int i = 0, j = 0;
	int size_image_height = 0;
	int size_image_width = 0;

	FREE_IMAGE_FORMAT format = FIF_UNKNOWN;
	BYTE* index = (BYTE*)malloc(sizeof(BYTE));


	if (dib) {

		size_image_height = FreeImage_GetHeight(dib);
		size_image_width = FreeImage_GetWidth(dib);

		if (FreeImage_GetBPP(dib) == 8) {
		/*
		  * Так как в файле bmp при 8-битном описании пикселя в пиксельных данных хранится значение индекса палитры,
		  * создадим новый индекс палитры:
		  * -если в ячейке массива бинаризации хранится 0 - запишем значение индекса палитры = 0, так как
		  * в нулевом индексе хранится черный цвет {0,0,0,0};
		  * -если в ячейке массива бинаризации хранится 1 - запишем значение индекса палитры = 255, так как
		  * в 255 индексе хранится белый цвет {255,255,255,0};
		  * */

			for (i = 0; i < size_image_height; i++) {
				for (j = 0; j < size_image_width; j++) {

					if (img_binary[i][j] == 0) {

						*index = 0;
						FreeImage_SetPixelIndex(dib, j, i, index);

					}
					else if (img_binary[i][j] == 1) {

						*index = 255;
						FreeImage_SetPixelIndex(dib, j, i, index);

					}
					else {
						printf("The binarization array contains values ​​other than 0 or 1!");
						return;
					}

				}
			}

			format = FreeImage_GetFileType(in_image_path, 0);

			if (FreeImage_Save(format, dib, out_image_path, 0)) {

				printf("Image saved successfully!\n");

			}
			else {

				printf("An error occurred while saving the image!");

			}
			
		}
		else {
			printf("Pixel depth is greater than 8 bits.Use an image with a pixel depth of 8bpp.");
			return;
		}

	}
	else {
		printf("Failed open file!\n");
		return;
	}
	
	FreeImage_Unload(dib);
	free(index);

}

double mean_local_area(int i, int j, int w, double** img_brightness, int number_of_columns_in_an_array_brightness) {

	int line_y = i;
	int line_x = j;
	double sum_for_mean = 0;
	double x_mean = 0;
	int number_of_members = w * w;

	for (line_y = i; line_y < w + i; line_y++) {

		for (line_x = j; line_x < w + j; line_x++) {

			sum_for_mean += img_brightness[line_y][line_x];

		}

	}

	x_mean = sum_for_mean / number_of_members;

	return x_mean;
}
double deviation_local_area(int i, int j, int w, double x_mean, double** img_brightness, int number_of_columns_in_an_array_brightness) {

	int line_y = i;
	int line_x = j;

	double sum_for_deviation = 0;
	double x_deviation = 0;
	int number_of_members = w * w;

	for (line_y = i; line_y < w + i; line_y++) {

		for (line_x = j; line_x < w + j; line_x++) {

			sum_for_deviation += pow((img_brightness[line_y][line_x] - x_mean), 2);

		}

	}

	x_deviation = sqrt(sum_for_deviation / number_of_members);

	return x_deviation;

}

void freeing_up_memory(void** A, size_t n) {

	for (int i = 0; i < n; i++) {
		free(A[i]);
	}
	free(A);
}

void dynamic_array_print_Char(char** array, size_t n, size_t m) {

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < 10; j++) {

			printf("[%d][%d] %d \n", i, j, array[i][j]);
		}
		//printf("\n");
		break;
	}
}

void dynamic_array_print_Double(double** array, size_t n, size_t m) {

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {

			printf("[%d][%d] %lf \n", i, j, array[i][j]);
		}
		//printf("\n");
		break;
		
	}
}

/**
FreeImage error handler
@param fif Format / Plugin responsible for the error
@param message Error message
*/
void FreeImageErrorHandler(FREE_IMAGE_FORMAT fif, const char* message) {
	printf("\n*** ");
	printf("%s Format\n", FreeImage_GetFormatFromFIF(fif));
	printf(message);
	printf(" ***\n");
}

// ----------------------------------------------------------

/** Generic image loader

  @param lpszPathName Pointer to the full file name
  @param flag Optional load flag constant
  @return Returns the loaded dib if successful, returns NULL otherwise
*/

FIBITMAP* GenericLoader(const char* lpszPathName, int flag)
{
	FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
	// check the file signature and deduce its format
	// (the second argument is currently not used by FreeImage)

	fif = FreeImage_GetFileType(lpszPathName, 0);

	if (fif == FIF_UNKNOWN)
	{
		// no signature ?
		// try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(lpszPathName);
	}

	// check that the plugin has reading capabilities ...
	if ((fif != FIF_UNKNOWN) && FreeImage_FIFSupportsReading(fif))
	{
		// ok, let's load the file
		FIBITMAP* dib = FreeImage_Load(fif, lpszPathName, flag);

		// unless a bad file format, we are done !
		return dib;
	}

	return NULL;
}
